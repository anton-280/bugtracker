CREATE TABLE users
(
  id           SERIAL                 NOT NULL,
  email        CHARACTER VARYING(100) NOT NULL,
  "password"   CHARACTER VARYING(128) NOT NULL,
  dynamic_salt TEXT                   NOT NULL, --FIXME salt
  name         VARCHAR(30) DEFAULT '',
  surname      VARCHAR(30) DEFAULT '',
  active       BOOLEAN     DEFAULT FALSE,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT unique_email UNIQUE (email)
) WITH (
OIDS = FALSE
);
ALTER TABLE users
  OWNER TO postgres;

CREATE TABLE projects
(
  id                SERIAL                      NOT NULL,
  name              CHARACTER VARYING(200)      NOT NULL,
  description       TEXT DEFAULT '',
  creation_datetime TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  edit_datetime     TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  project_creator   INTEGER                     NOT NULL,
  CONSTRAINT projects_pkey PRIMARY KEY (id),
  CONSTRAINT project_creator_fk FOREIGN KEY (project_creator)
  REFERENCES users (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
);
ALTER TABLE projects
  OWNER TO postgres;

CREATE TABLE versions
(
  id          SERIAL                NOT NULL,
  project_id  INTEGER               NOT NULL,
  name        CHARACTER VARYING(50) NOT NULL,
  description TEXT DEFAULT '',
  CONSTRAINT versions_pkey PRIMARY KEY (id),
  CONSTRAINT project_id_fk FOREIGN KEY (project_id)
  REFERENCES projects (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (
OIDS = FALSE
);
ALTER TABLE users
  OWNER TO postgres;

CREATE TABLE priorities
(
  id   SERIAL                NOT NULL,
  name CHARACTER VARYING(20) NOT NULL,
  lang CHARACTER VARYING(5)  NOT NULL,
  CONSTRAINT priorities_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);
ALTER TABLE priorities
  OWNER TO postgres;

INSERT INTO priorities (name, lang)
VALUES ('Major', 'EN'), ('Minor', 'EN'), ('Trivial', 'EN'), ('Critical', 'EN'), ('Blocker', 'EN'), ('Flaming', 'EN');

CREATE TABLE status
(
  id   SERIAL NOT NULL,
  name CHARACTER VARYING(25),
  lang CHARACTER VARYING(5),
  CONSTRAINT status_pkey PRIMARY KEY (id)
) WITH (
OIDS = FALSE
);
ALTER TABLE users
  OWNER TO postgres;

INSERT INTO status (name, lang)
VALUES ('New', 'EN'), ('In work', 'EN'), ('Fixed', 'EN'), ('Not fixed', 'EN'), ('Reopen', 'EN'), ('Closed', 'EN'),
  ('Duplicate', 'EN'), ('irreproducible', 'EN');

CREATE TABLE bugs (
  id                SERIAL                      NOT NULL,
  project_id        INTEGER                     NOT NULL,
  name              VARCHAR(100)                NOT NULL,
  description       TEXT                        NOT NULL,
  bug_discoverer    INTEGER                     NOT NULL,
  creation_time     TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  version           INTEGER                     NOT NULL,
  priority          INTEGER                     NOT NULL,
  assign_to_fix     INTEGER,
  status            INTEGER                     NOT NULL,
  version_bug_fixed INTEGER,
  CONSTRAINT bugs_pkey PRIMARY KEY (id),
  CONSTRAINT user_bug_discoverer_fk FOREIGN KEY (bug_discoverer)
  REFERENCES users (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT user_assign_to_fix_fk FOREIGN KEY (assign_to_fix)
  REFERENCES users (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT project_id_fk FOREIGN KEY (project_id)
  REFERENCES projects (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT version_id_fk FOREIGN KEY (version)
  REFERENCES versions (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT version_bug_fixed_fk FOREIGN KEY (version_bug_fixed)
  REFERENCES versions (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT status_fk FOREIGN KEY (status)
  REFERENCES status (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT priority_fk FOREIGN KEY (priority)
  REFERENCES priorities (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (
OIDS = FALSE
);
ALTER TABLE bugs
  OWNER TO postgres;