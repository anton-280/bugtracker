CREATE TABLE sys_config (
  id   SERIAL                NOT NULL,
  name CHARACTER VARYING(20) NOT NULL,
  val  CHARACTER VARYING(20),
  CONSTRAINT sys_config_pkey PRIMARY KEY (id)
);

INSERT INTO sys_config (name, val) VALUES ('name', 'Insider');
INSERT INTO sys_config (name, val) VALUES ('last_patch', '0');