package me.kapustin.bugs.rest.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Entity
@Table(name="versions")
public class VersionEntity extends AbstractEntity {

	public VersionEntity() {
	}

	@Getter @Setter private String description;

	@Getter @Setter private String name;

	@ManyToOne
	@Getter @Setter private ProjectEntity project;
}