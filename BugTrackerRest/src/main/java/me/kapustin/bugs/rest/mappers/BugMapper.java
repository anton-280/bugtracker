package me.kapustin.bugs.rest.mappers;

import me.kapustin.bugs.rest.entity.BugEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Mapper
public interface BugMapper {

    BugMapper INSTANCE = Mappers.getMapper(BugMapper.class);

    static BugMapper getInstance() {
        return INSTANCE;
    }

    //   @Mapping(source = "numberOfSeats", target = "seatCount")
    // CarDto carToCarDto(Car car); 2
}
