package me.kapustin.bugs.rest.security.auth;

/**
 * @author Kapustin Anton anton@kapustin.me
 */

import me.kapustin.bugs.rest.exception.DataCorruptedException;
import me.kapustin.bugs.rest.service.AuthService;
import me.kapustin.bugs.rest.service.TokenService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


public class TokenAuthenticationFilter extends GenericFilterBean {
    
    private static final String HEADER_TOKEN = "X-Auth-Token";
    
    private static final Logger log = Logger.getLogger(TokenAuthenticationFilter.class);
    
    @Autowired
    TokenService tokenService;
    
    @Autowired
    AuthService authService;

    SecurityContext securityContext = SecurityContextHolder.getContext();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        try {
            if (securityContext.getAuthentication() == null) {
                String encodedToken = httpRequest.getHeader(HEADER_TOKEN);
                Token token = tokenService.decryptToken(encodedToken);
                if (authService.authenticateWithToken(token)) {
                    List<GrantedAuthority> authorities = new ArrayList<>();
                    GrantedAuthority role = new SimpleGrantedAuthority("USER");
                    authorities.add(role);
                    UserDetails userDetails = new User(token.getEmail(),
                            token.getPassword(), authorities);
                    Authentication securityToken = new PreAuthenticatedAuthenticationToken(userDetails, null,
                            userDetails.getAuthorities());
                    securityContext.setAuthentication(securityToken);
                } else {
                    writeResponse(httpResponse, HttpServletResponse.SC_UNAUTHORIZED, "Authentication failed, invalid credentials.");
                }
            }
        } catch (DataCorruptedException dce) {
            writeResponse(httpResponse, HttpServletResponse.SC_BAD_REQUEST, "Token invalid or corrupted.");
        } catch (IllegalArgumentException iae){
            writeResponse(httpResponse, HttpServletResponse.SC_UNAUTHORIZED, "Authentication failed, no token.");
        }
        chain.doFilter(request, response);
    }

    private void writeResponse(HttpServletResponse response, int status, String msg) throws IOException {
        response.setStatus(status);
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        Writer out = response.getWriter();
        out.write(msg);
        out.close();
    }
}
