package me.kapustin.bugs.rest.service.impl;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import me.kapustin.bugs.rest.exception.DataCorruptedException;
import me.kapustin.bugs.rest.security.auth.Token;
import me.kapustin.bugs.rest.service.TokenService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Service
public class TokenServiceImpl implements TokenService {

    private static final String KEY = RandomStringUtils.randomAscii(16);

    AesCipherService aes = new AesCipherService();

    @SneakyThrows
    @Override
    public String encryptToken(Token token) {
        ByteSource encrypt = aes.encrypt(token.toString().getBytes(Charset.forName("UTF8")), KEY.getBytes(Charset.forName("UTF8")));
        return Base64.getEncoder().encodeToString(encrypt.getBytes());
    }

    @Override
    public Token decryptToken(String cryptedToken) {
        if (cryptedToken == null || cryptedToken.equals("")) {
            throw new IllegalArgumentException("Encrypt token must be not null/empty string.");
        }
        ByteSource decrypt = aes.decrypt(Base64.getDecoder().decode(cryptedToken), KEY.getBytes(Charset.forName("UTF8")));
        Token token = new Gson().fromJson(new String(decrypt.getBytes(), Charset.forName("UTF8")), Token.class);
        if (isHashValid(token)) {
            return token;
        } else {
            throw new DataCorruptedException("Token corrupted.");
        }
    }

    @Override
    public String tokenHash(Token token) {
        return Hashing.goodFastHash(32).hashString(token.getPassword()
                + token.getEmail() + token.getLoginTime(), Charset.forName("UTF8")).toString();
    }

    @Override
    public Boolean isHashValid(Token token) {
        return token.getHash().equals(tokenHash(token));
    }


}
