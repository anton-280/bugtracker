package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.ProjectEntity;
import me.kapustin.bugs.rest.entity.VersionEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface VersionRepository extends AbstractRepository<VersionEntity> {
    VersionEntity findVersionByNameAndProject(String name, ProjectEntity project);
}
