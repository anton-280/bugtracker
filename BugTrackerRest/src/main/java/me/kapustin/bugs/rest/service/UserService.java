package me.kapustin.bugs.rest.service;

import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;

import java.util.Optional;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
public interface UserService {
    UserEntity registerUser(UserDTO user);
    Optional<UserEntity> getById(Integer id);
}