package me.kapustin.bugs.rest.utils;


import java.io.*;
import java.net.URL;

public final class IOUtils {
    private IOUtils() {
    }

    public static String readFile(Class<?> clazz, String fileName) throws IOException {
        byte[] readBytes = readFileBytes(clazz, fileName);
        String result = null;
        if (readBytes != null) {
            result = new String(readBytes);
        }
        return result;
    }

    public static String readFile(InputStream in) throws IOException {
        byte[] readBytes = readFileBytes(in);
        String result = null;
        if (readBytes != null) {
            result = new String(readBytes);
        }
        return result;
    }

    public static String readFile(URL url) throws IOException {
        byte[] readBytes = readFileBytes(url);
        String result = null;
        if (readBytes != null) {
            result = new String(readBytes);
        }
        return result;
    }

    public static byte[] readFileBytes(Class<?> clazz, String fileName) throws IOException {
        InputStream in = null;
        try {
            byte[] result = null;
            in = clazz.getResourceAsStream(fileName);
            if (in != null) {
                result = readFileBytes(in);
            }
            return result;
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public static byte[] readFileBytes(URL url) throws IOException {
        InputStream in = null;
        try {
            byte[] result = null;
            in = url.openStream();
            if (in != null) {
                result = readFileBytes(in);
            }
            return result;
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public static byte[] readFileBytes(File file) throws IOException {
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            return readFileBytes(in);
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public static byte[] readFileBytes(InputStream in) throws IOException {
        int read;
        byte[] buf = new byte[1024];
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        while ((read = in.read(buf)) != -1) {
            bytes.write(buf, 0, read);
        }
        byte[] result = bytes.toByteArray();
        return result;
    }
}
