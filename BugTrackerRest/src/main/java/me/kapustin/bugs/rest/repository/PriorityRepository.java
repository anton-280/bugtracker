package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.PriorityEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface PriorityRepository extends AbstractRepository<PriorityEntity> {
    PriorityEntity findPriorityByName(String name);
}
