package me.kapustin.bugs.rest.dto;

import lombok.Getter;
import lombok.Setter;
import me.kapustin.bugs.rest.entity.BugEntity;
import me.kapustin.bugs.rest.entity.ProjectEntity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class UserDTO {

    @Getter @Setter int id;

    @NotNull
    @Pattern(regexp = "^(?:[a-zA-Z0-9_'^&/+-])+(?:\\.(?:[a-zA-Z0-9_'^&/+-])+)" +
            "*@(?:(?:\\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\\.)" +
            "{3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\]?)|(?:[a-zA-Z0-9-]+\\.)" +
            "+(?:[a-zA-Z]){2,}\\.?)$",
            message = "Email must be valid.")
    @Getter @Setter private String email;

    @NotNull
    @Getter @Setter private String name;


    @NotNull
    @Getter @Setter private String surname;

    @NotNull
    @Size(min = 8, message = "Passwords must be at least 8 characters in length.")
    @Pattern(regexp = "^((?=.*\\d)(?=.*([a-z]|[a-я]))(?=.*([A-Z]|[А-Я])).[\\S]+)$",
    message = "Password must consist of upper and lower case letters.")
    @Getter @Setter private String password;

    @Getter @Setter private List<BugEntity> opened;

    @Getter @Setter private List<BugEntity> assignedToFix;

    @Getter @Setter private List<ProjectEntity> projects;

}
