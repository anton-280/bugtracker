package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface UserRepository extends AbstractRepository<UserEntity> {
    Optional<UserEntity> findUserByEmail(String email);
    Optional<UserEntity> findUserById(Integer id);
}
