package me.kapustin.bugs.rest.entity;

import lombok.Getter;
import lombok.Setter;
import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.security.Saltable;

import javax.persistence.*;
import java.util.List;


/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Entity
@Table(name="users")
public class UserEntity extends AbstractEntity implements Saltable{

	public UserEntity() {
	}

	@Getter @Setter private boolean active;

	@Getter @Setter private String email;

	@Getter @Setter private String name;

	@Getter @Setter private String surname;

	@Column(name = "dynamic_sald")//FIXME
	@Getter @Setter private String salt;

	@Column(name="password")
	@Getter @Setter private String passwordHash;

	@OneToMany(mappedBy="discoverer", fetch=FetchType.EAGER)
	@Getter @Setter private List<BugEntity> opened;

	@OneToMany(mappedBy="assignFixer", fetch=FetchType.EAGER)
	@Getter @Setter private List<BugEntity> assignedToFix;

	@OneToMany(mappedBy="creator", fetch=FetchType.EAGER)
	@Getter @Setter private List<ProjectEntity> projects;

	public UserDTO toUserDTO(){
		UserDTO user = new UserDTO();
		user.setName(name);
		user.setSurname(surname);
		user.setEmail(email);
		user.setAssignedToFix(assignedToFix); //FIXME:!!!!
		user.setOpened(opened);
		user.setProjects(projects);
		user.setId(getId());
		return user;
	}

}