package me.kapustin.bugs.rest.entity;

import lombok.Getter;
import lombok.Setter;
import me.kapustin.bugs.rest.utils.HashCodeUtils;

import javax.persistence.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter @Setter private Integer id;

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (!(other instanceof AbstractEntity)) return false;

        final AbstractEntity ent = (AbstractEntity) other;

        if (!ent.getId().equals(getId())) return false;

        return true;
    }

    //FIXME: NEED RESEARHcE
    @Override
    public int hashCode() {
        Integer id = getId();
        return HashCodeUtils.generateHash(id);
    }

}