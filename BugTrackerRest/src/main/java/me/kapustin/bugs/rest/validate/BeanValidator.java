package me.kapustin.bugs.rest.validate;

import jdk.nashorn.internal.runtime.options.Option;

import java.util.Optional;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
public interface BeanValidator {
   void validate(Object object);
}
