package me.kapustin.bugs.rest.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;


/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Entity
@Table(name="bugs")
public class BugEntity extends AbstractEntity {

    public BugEntity() {
    }

	@PrePersist
	private void prePersist() {
		creationTime = Instant.now();
	}

	@Column(name="creation_time")
	@Getter private Instant creationTime;

	@Getter @Setter private String name;

	@Getter @Setter private String description;

	@ManyToOne
	@JoinColumn(name="priority")
	@Getter @Setter private PriorityEntity priority;

	@ManyToOne
    @Getter @Setter private ProjectEntity project;

	@ManyToOne
	@JoinColumn(name="status")
    @Getter @Setter private StatusEntity statusBean;

    @ManyToOne
    @JoinColumn(name="bug_discoverer")
    @Getter @Setter private UserEntity discoverer;

	@ManyToOne
	@JoinColumn(name="assign_to_fix")
    @Getter @Setter private UserEntity assignFixer;

	@ManyToOne
	@JoinColumn(name="version_bug_fixed")
    @Getter @Setter private VersionEntity fixed;

	@ManyToOne
	@JoinColumn(name="version")
	@Getter @Setter private VersionEntity foundIn;
}