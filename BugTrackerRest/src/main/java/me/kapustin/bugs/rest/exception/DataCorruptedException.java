package me.kapustin.bugs.rest.exception;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class DataCorruptedException extends RuntimeException {
    public DataCorruptedException() {
    }

    public DataCorruptedException(String message) {
        super(message);
    }

    public DataCorruptedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataCorruptedException(Throwable cause) {
        super(cause);
    }

    public DataCorruptedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
