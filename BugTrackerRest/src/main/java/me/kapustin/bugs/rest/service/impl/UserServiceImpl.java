package me.kapustin.bugs.rest.service.impl;

import lombok.SneakyThrows;
import me.kapustin.bugs.rest.service.UserService;
import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.exception.UserCreationException;
import me.kapustin.bugs.rest.repository.UserRepository;
import me.kapustin.bugs.rest.security.crypto.hash.Hasher;
import me.kapustin.bugs.rest.validate.BeanValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Optional;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepo;

    @Autowired
    BeanValidator validator;

    @Autowired
    Hasher hasher;


    @Override
    @SneakyThrows
    public UserEntity registerUser(UserDTO user) {
        validator.validate(user);
        Optional<UserEntity> exists = userRepo.findUserByEmail(user.getEmail());
        if (!exists.isPresent()){
            UserEntity toRegister = new UserEntity();
            toRegister.setName(user.getName());
            toRegister.setSurname(user.getSurname());
            toRegister.setEmail(user.getEmail());
            SecureRandom random = new SecureRandom();
            byte[] saltBytes = new byte[random.nextInt(877) + 256];
            random.nextBytes(saltBytes);
            toRegister.setSalt(new String(saltBytes, "UTF8"));
            toRegister.setPasswordHash(hasher.hash(user.getPassword(), toRegister));
            return userRepo.save(toRegister);
        } else {
            throw new UserCreationException("User with email: " + exists.get().getEmail() + " already exists!");
        }
    }

    @Override
    public Optional<UserEntity> getById(Integer id){
        return userRepo.findUserById(id);
    }

}
