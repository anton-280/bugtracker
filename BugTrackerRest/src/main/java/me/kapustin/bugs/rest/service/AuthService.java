package me.kapustin.bugs.rest.service;

import me.kapustin.bugs.rest.security.auth.Token;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface AuthService {
    Token generateToken(String username, String password);

    Boolean authenticateWithToken(Token token);
}
