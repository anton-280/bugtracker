package me.kapustin.bugs.rest.exception;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class NoSuchBugException extends RuntimeException{
    public NoSuchBugException() {
    }

    public NoSuchBugException(String message) {
        super(message);
    }

    public NoSuchBugException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchBugException(Throwable cause) {
        super(cause);
    }

    public NoSuchBugException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
