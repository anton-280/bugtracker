package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.controller.AuthRestController;
import me.kapustin.bugs.rest.security.auth.Token;
import me.kapustin.bugs.rest.service.AuthService;
import me.kapustin.bugs.rest.service.TokenService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RestController
@RequestMapping("/")
public class AuthRestControllerImpl implements AuthRestController {

    private static final Logger log = Logger.getLogger(AuthRestControllerImpl.class);

    @Autowired
    AuthService authService;

    @Autowired
    TokenService tokenService;

    @Override
    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public String authenticate(String email, String password, HttpServletResponse res) {
        try {
            Token token = authService.generateToken(email, password);
            res.setStatus(HttpStatus.OK.value());
            return tokenService.encryptToken(token);
        } catch (AuthenticationServiceException ase) {
            res.setStatus(HttpStatus.UNAUTHORIZED.value());
            res.setHeader("Warning", ase.getMessage());
            return "";
        }
    }
}
