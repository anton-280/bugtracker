package me.kapustin.bugs.rest.exception;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
public class BeanValidationException extends RuntimeException {

    public BeanValidationException(String message) {
        super(message);
    }

    public BeanValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanValidationException(Throwable cause) {
        super(cause);
    }

    public BeanValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
