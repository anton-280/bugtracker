package me.kapustin.bugs.rest.controller;

import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.entity.BugEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface BugRestController {
    List<BugEntity> getUsersBugs();

    void openBug(CreateBugDTO bugDTO, HttpServletRequest req, HttpServletResponse res);

    BugEntity getBug(Integer id, HttpServletResponse mockRes);
}
