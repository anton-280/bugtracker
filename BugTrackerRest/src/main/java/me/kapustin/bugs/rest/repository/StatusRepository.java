package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.StatusEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface StatusRepository extends AbstractRepository<StatusEntity> {
    StatusEntity findStatusByName(String name);
}
