package me.kapustin.bugs.rest.controller;

import me.kapustin.bugs.rest.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
public interface UserRestController {
    void createUser(UserDTO user, HttpServletRequest httpReq,
                    HttpServletResponse httpRes);

    UserDTO getUserById(Integer id, HttpServletResponse httpRes);
}
