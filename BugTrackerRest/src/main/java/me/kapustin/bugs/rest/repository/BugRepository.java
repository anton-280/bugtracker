package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.BugEntity;
import me.kapustin.bugs.rest.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface BugRepository extends AbstractRepository<BugEntity> {
    Optional<BugEntity> findOne(int id);
}