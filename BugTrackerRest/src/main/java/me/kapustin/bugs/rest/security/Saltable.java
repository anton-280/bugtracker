package me.kapustin.bugs.rest.security;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface Saltable {
    String getSalt();
}
