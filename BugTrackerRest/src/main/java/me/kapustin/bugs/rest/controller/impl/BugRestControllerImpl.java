package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.controller.BugRestController;
import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.BugEntity;
import me.kapustin.bugs.rest.exception.BeanValidationException;
import me.kapustin.bugs.rest.exception.NoSuchBugException;
import me.kapustin.bugs.rest.service.BugService;
import me.kapustin.bugs.rest.validate.BeanValidator;
import me.kapustin.bugs.rest.validate.impl.JavaxBeanValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RestController
@RequestMapping("/rest")
public class BugRestControllerImpl implements BugRestController {

    @Autowired
    BugService bugService;

    @Autowired
    BeanValidator beanValidator;

    @Override
    @RequestMapping(value = "/bug", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<BugEntity> getUsersBugs() {
        List<BugEntity> bugs = bugService.getOpenedBugs();
        if(bugs != null) {
            return bugs;
        } else {
            return  Collections.emptyList();
        }
    }

    @Override
    @RequestMapping(value = "/bug", method = RequestMethod.POST)
    public void openBug(@RequestBody CreateBugDTO bugDTO, HttpServletRequest req, HttpServletResponse res) {
        try {
            beanValidator.validate(bugDTO);
            BugEntity bug = bugService.createBug(bugDTO);
            res.setHeader("Location", req.getRequestURI().replace("open", String.valueOf(bug.getId())));
            res.setStatus(HttpStatus.CREATED.value());
        } catch (BeanValidationException bve){
            res.setHeader("Warning", bve.getMessage());
            res.setStatus(HttpStatus.BAD_REQUEST.value());
        }
    }


    @Override
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public @ResponseBody BugEntity getBug(@PathVariable Integer id, HttpServletResponse res) {
        res.setStatus(HttpStatus.OK.value());
        return bugService.getBugById(id).orElseThrow(() -> {
            res.setStatus(HttpStatus.BAD_REQUEST.value());
            res.setHeader("Warning", "No bean with id:" + id + "exists.");
            return new NoSuchBugException("");
        });
    }
}
