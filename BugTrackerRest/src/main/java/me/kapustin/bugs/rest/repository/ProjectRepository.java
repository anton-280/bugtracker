package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.ProjectEntity;
import org.springframework.stereotype.Repository;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Repository
public interface ProjectRepository extends AbstractRepository<ProjectEntity> {
}
