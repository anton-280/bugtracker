package me.kapustin.bugs.rest.db;

import me.kapustin.bugs.rest.utils.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class DbUpdater {

    public DbUpdater(DataSource ds, String path) {
        dataSource = ds;
        this.path = path;
    }

    private DataSource dataSource;
    private String path;

    private Connection connection;
    private static final String PATCH_PREFIX = "patch_";
    private static final String PATCH_POSTFIX = ".sql";
    private static final Pattern versionPattern = Pattern.compile(".*/patch_([0-9]+)\\.sql");
    private static final String LAST_PATCH_SELECT_QUERY = "SELECT * FROM sys_config WHERE name = 'last_patch'";
    private static final String LAST_PATCH_UPDATE_QUERY = "UPDATE sys_config SET val = ? WHERE name = 'last_patch'";

    private static final Logger log = Logger.getLogger(DbUpdater.class);

    @PostConstruct
    public void run() {
        log.info("Executing patches...");
        try {
            connection = dataSource.getConnection();
            int last = 0;
            try {
                last = getCurrentPatchNo();
            } catch (SQLException e) {
                String message = e.getMessage();
                if (message.contains("relation \"sys_config\" does not exist")) {
                    InputStream input = getCurrentPatch(last);
                    if (input != null) {
                        runPatch(last, input);
                        last++;
                    }
                }
            }
            for (int i = last; i < 100; i++) {
                InputStream input = getCurrentPatch(i);
                if (input != null) {
                    runPatch(i, input);

                } else {
                    break;
                }
            }
        } catch (SQLException e) {
            log.error(e);
        } catch (IOException e) {
            log.error(e);
        } finally {
            close(connection);
        }
    }

    private void runPatch(int i, InputStream input) throws UnsupportedEncodingException, IOException, SQLException {
        String patchNo = String.format("%05d", i);
        String fullName = path + PATCH_PREFIX + patchNo + PATCH_POSTFIX;
        BufferedReader in = new BufferedReader(new InputStreamReader(input, "UTF8"));

        String queries;
        StringBuilder sb = new StringBuilder();
        while ((queries = in.readLine()) != null) {
            sb.append(queries);
            sb.append(" ");
            System.out.println(queries);
        }
        Statement stmt = connection.createStatement();
        stmt.execute(sb.toString());
        stmt.close();
        saveLastPatch(i);
    }

    private void saveLastPatch(int i) throws SQLException {
        PreparedStatement prepareStatement = connection.prepareStatement(LAST_PATCH_UPDATE_QUERY);
        prepareStatement.setString(1, String.valueOf(i));
        prepareStatement.execute();
    }

    private InputStream getCurrentPatch(int i) {
        String patchNo = String.format("%05d", i);
        String fullName = path + PATCH_PREFIX + patchNo + PATCH_POSTFIX;
        InputStream input = this.getClass().getResourceAsStream(fullName);
        return input;
    }

    private void applyPatch(InputStream input) throws IOException, SQLException {
        String queries = IOUtils.readFile(input);
        Statement stmt = connection.createStatement();
        for (String query : queries.split(";")) {
            stmt.execute(query);
            log.info(query);
        }
        stmt.close();
    }

    private int getCurrentPatchNo() throws SQLException {
        Statement createStatement = connection.createStatement();
        ResultSet result = createStatement.executeQuery(LAST_PATCH_SELECT_QUERY);
        result.next();
        String lastPatch = result.getString("val");
        int lastPatchNo = Integer.parseInt(lastPatch);
        int i = lastPatchNo + 1;
        createStatement.close();
        return i;
    }

    protected int getVersion(URL patch) {
        Matcher matcher = versionPattern.matcher(patch.getFile());
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The patch URL is not valid format for retriving version.");
        }
        String version = matcher.group(1);
        return Integer.parseInt(version);
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            log.error(e);
        }
    }
}
