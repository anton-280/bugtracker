package me.kapustin.bugs.rest.security.auth;

import com.google.gson.Gson;
import lombok.*;
import lombok.experimental.Builder;

import java.time.Instant;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Builder
@EqualsAndHashCode
public class Token {

    //TODO: Add Role and IP

    @Getter @Setter String email;
    @Getter @Setter String password;
    String loginTime;
    @Getter @Setter String hash;

    public Instant getLoginTime() {
        return Instant.parse(loginTime);
    }

    public void setLoginTime(Instant loginTime) {
        this.loginTime = loginTime.toString();
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
