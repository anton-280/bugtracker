package me.kapustin.bugs.rest.repository;

import me.kapustin.bugs.rest.entity.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface AbstractRepository<T extends AbstractEntity> extends JpaRepository<T, Integer> {

}