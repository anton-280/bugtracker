package me.kapustin.bugs.rest.security.crypto.hash.impl;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import me.kapustin.bugs.rest.security.Saltable;
import me.kapustin.bugs.rest.security.crypto.hash.Hasher;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestExecutionListeners;

import java.nio.charset.Charset;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Component
public class SecureSaltHasher implements Hasher {

    private static final String STATIC_SALT = "⁺\uED44偙唡蚤樱Ƕ베\uD820\uDC3C\u9FD3\uEAC3" +
            "嘂䕳\uE99A\uD856\uDC13⸈졆㐌㌚ꍱ鉊曭憼渻稜옆淶홞䞿՛⚝\uE0CA㎁즽\uE169\uF019ⳬɇ⎶홗껡엽쯐藷뚿뎉" +
            "\uF0F1꿙⧧\uF117俒뱾軡\u0A58맢깕‡છ鴠\uD80B\uDE72儏媉誤\uE8F5봞섵ꠙÎ\uF0ECꐌ픲빤蟴‗쯲鹋㿋쥾" +
            "ᶼꯉヵ锖綁\u2EFA嗭\uF49B\uE310撘븻㮉㖄\uE562\uEBB6ʊ䫏̠\uD861\uDF94⩭ŉ㽩梚ꄭ꽉燄ᛥ吡詫˭䄆ꂮⵘᵄ" +
            "Ų랶獁冊羲臩咧挂눽דּ⎼궫们퍇湭쒡FUCK氈YOU궫HACKERڜ鐭ᣃ籽ڈ繄\uD828\uDF4A왋鳖㎫柿\uE19D嵳\uEE59陣\uE709\uF2F3" +
            "鉜ᠹ㴊\uF449돯媫ᅉ姼⠈煊\uEFFB〹\uE903㢷셟\uF38FⱲ㫪ଢ଼\uD809\uDC7C笔畁谸ꎮ\uEF2B\uAB9E㮥嚧ഋ\uE248ါ" +
            "귤焼割纹饴䕜㋮⠅鼍갡좋䞘侼ᤃ䲐\uD805\uDEB4\u197D蝞ࣦܺ뗶ᴫ⡺辵ぉꫣ\uF894ퟛ藥\uF122쌤\uFFFF뮚歔墙坿飺탵Ꝓ\uE053ᥠừ" +
            "枹햺碗\uE709틵䐅愢쎧\uD872\uDF8D戛\uED66\uF4E7梺狃ᄇ뫋\u0B31\uE99Eꍲ↬䘧\uED40ᇌዴோұ䮹뀱\uE5D6൱䖺방" +
            "ഷ素ㆨ㺑苣瓄䭖\uF860埁쮰\uD85C\uDECB\uD806\uDC07ᆧ狗㯥嫡欖殍쿙阛釷죗䘕ㆮꖘӮﮒ㛁ᆒ면멲ى⽈딬폈텑\uEB84͋墏룠渺㵲뾰" +
            "孾\uDB43\uDC25朴㙛鯩ક纑\uE015隣\u1F4E࠘쌜苭凓뾃\uD910\uDC04∥펤궾䓄喓脲ප덐┐벵槗➍툩㦚튗뫉碜㱙\u12B7毆銏엊輛᧕䕤얐" +
            "嘨⨔\u2D2E↤\uA4CE瓏ﳉ脋윬㣺\uE893\uDAD8\uDC14\uF3D4熻僮飂\u1A9F젺顲\uECBD풃饮\u1C90삢\uF28B셈ꬂę莿\uF426搹臽" +
            "荩ᝉ揲\u0862㽫鷬᳢怾㓅\uE0F7畿Ϋ䀬ݗ폷种\u20CD듳朵㍜欩噅ᬖ䫇⨓纊얳ⷻ词溟㯏\uD7C7᥈穲\uD84D\uDEE6ᖁꌙ㍥䂡쌁宋\uAB5C洗" +
            "浵\u0DF1\uF68A묢㕴鞟˟য়뱗醬ﴦ\uF5D2\uD867\uDC23㽚\uA7AC䠘臜ᘏ\uE2B3᠍ꫴ⬅飅喭\uF3DD苐\uEA00킉Ἓᮬᘉ鋊랔孥" +
            "蓯\uD98A\uDC36驫궴ި\uF830㧼䁰ﳛᗝ퍲騚邾蓩⺴⟬且\uE71B췁j\uE08A쇄ﭠ須갻鱳櫬\u176Dै䁍靃祠↙一⻗㙅躸\\䀹\uE34C\uF5D4醆" +
            "콹쬰ￛ壒㍻⚟\u2BB9圳貛㩃ᢵ衙訃ɢ꜡솃̜堞樎侘⟭㌻짐棠\uE7B1쫨廬⪎䤧辸\u0DFB䒣흢\uD984\uDC04ꕙ渣쓆\uE270榴ꯧ势\uED22ı\uEF8Aᰯ愨銐" +
            "析\uE2D5妙쿅夅詣ﰄ蜃搉፤\uEC0A廰\u2BF2쐲冑䯬\uD87B\uDE18螝沃ਪ岏⧅\u1AFC\uAB54岜홆∁䣂\uEA4D骿≵닾ቜꆂ塜\uEAC8㗤喋೭⪥빈" +
            "팘⎼Ђ漬\u1ADD儋闸뒒ᷙꜷ莳欞埒䥇\uF07C쁮乡゙杗\uE998\uEAB3Ꭾ㑑跏䏕鰜啢\uE319ྲ㦚죳鬯那.\uD867\uDE80疫\uD9F7\uDC57덇썆ꄱ꿪仏" +
            "竤㲔闊\uEED1囿ⴵ훳\uF8ED野\u1AB6呶㹌䲝㾾謀慞췃ḆꙞ箶欷ⶃࢧ䈋韇ᒻ䫾ђ⣭ᠫ巺\u07FF譈軞\uDA62\uDC1D㮬\uD877\uDD45夦竞︊" +
            "陚们\uDA00\uDC23\uF7CDꈎ鴌鴬ђ㍸獟零肓\uABA9\uE0FD⁃飸鏏觝㯣圏㤈歈Ⱐⲍ\uF7BC釳\uEB6A秶괠㪮阦雧Ⳏ\uF3BF\uF39D臶뢇\uF4D7氉" +
            "︺頒\uD9C3\uDC01葵\uDB19\uDC12쎑ී\u0FDBꬠ긔幖♇\uE732剗ᓘ㵽㥡Ꝼ暴醴ည춅ഊ\uD80B\uDC30ݑ\uDA24\uDC38뙲ᆙ覄脟䂞" +
            "䔧쫹䊎㧨ሡ赺\uE892艻ヺ똙땑酿䫺\u243E宁䚀ᜪꗇ\uE3BEꂥ즯仂뽇臙\uE909幁\u2D28\uE6D8疚젼\uDA5D\uDC7D䓍\uDB35\uDC53녯闘倏" +
            "\uF4D1㻓蘧烘痱쮔\uAB60\uA69D毠습뤅稭䢖Ḁ럂\uDB10\uDC2D쇰\uF44C븺ๆᛁ毩䤅䒿䲡\uDA10\uDC25\uD8F7\uDC4A㻚㩯\uDAD8\uDC04༊胡" +
            "덟㒑汴䄶⠊䡗㏰ꇃ筟\uD83E\uDC2F㔈瞖ᡒ禚镫礖鹒\uEFDD\uE114윃į뜎叱\u2E79刴㩭巍\uF660쁆盏늑\uE1E5剳ả\uF2A4鞋㖡⦘ꥦ拣\uAB5A嗡" +
            "翐\uE6E6잻섟ꄰ\uF6E5䲅廢瀈氣ང닔줎済碣푻㉅䋺쫚抡瑂忑ﴶ걙㮄௮\u1ACF\uF5A8翳述㉐巆邀蔌Ө贈샥笀쉗\uF064흻쪬䀛\uE899祁\u0B45㧀" +
            "\uF2F1\uF1F5䀯䡏熬홒畭廖娂ṱ\u20BF먞귌癄㵑关䛉ꈵ렓\uF694ᦁ\uF5E6抲狃뗏\uFB09䌪\u0EF0欈虧뗽ᔇᖻ飶礋\uF861㴜講唇\uF34E옊 뮌߉↱㼶" +
            "俒焽緕㈋\uF008땻\u1CB4掏畇ꋇ骏쁏(\uA8FF蛍\uF45D䟺᯳囅\uE567ʜೌ堞\uE6FB\u1AEA袜\uE8DD䛤\u0BDF\uDB02\uDC5D\uEFA6嘿ఫ觗＝" +
            "꛷䱚원似\uE40B楝搐\u0EE9\uF633\uED85슕\uA8FD씊\uF202槻坡篬⥍俞\uE04D쵣㺺ᓦᮢ譅礣ᵬ\uFE6E襥꺸\u0BBCઈ";

    @Override
    public String hash(String pass, Saltable salt) {
        return hash(pass, salt.getSalt());
    }

    @Override
    public String hash(String pass, String salt) {
        HashFunction hashFunction = Hashing.sha512();
        return hashFunction.hashString(pass + salt + STATIC_SALT +
                hashFunction.hashString(salt, Charset.forName("UTF8")), Charset.forName("UTF8")).toString();
    }

}
