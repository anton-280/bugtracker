package me.kapustin.bugs.rest.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Entity
@Table(name="projects")
public class ProjectEntity extends AbstractEntity {

	public ProjectEntity() {
	}

	@Column(name="creation_datetime")
	@Getter @Setter private Timestamp creationDatetime;

	@Getter @Setter private String description;

	@Column(name="edit_datetime")
	@Getter @Setter private Timestamp editDatetime;

	@Getter @Setter private String name;

	@OneToMany(mappedBy="project", fetch=FetchType.EAGER)
	@Getter @Setter private List<BugEntity> bugs;

	@ManyToOne
	@JoinColumn(name="project_creator")
	@Getter @Setter private UserEntity creator;

	@OneToMany(mappedBy="project", fetch=FetchType.EAGER)
	@Getter @Setter private List<VersionEntity> versions;
}