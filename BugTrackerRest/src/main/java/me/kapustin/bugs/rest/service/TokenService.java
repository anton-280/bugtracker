package me.kapustin.bugs.rest.service;

import lombok.SneakyThrows;
import me.kapustin.bugs.rest.security.auth.Token;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface TokenService {

    String encryptToken(Token token);

    Token decryptToken(String cryptedToken);

    String tokenHash(Token testToken);

    Boolean isHashValid(Token token);
}
