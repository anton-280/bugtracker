package me.kapustin.bugs.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface AuthRestController {
    String authenticate(String login, String password, HttpServletResponse res);
}
