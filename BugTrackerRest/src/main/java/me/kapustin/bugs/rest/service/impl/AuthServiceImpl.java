package me.kapustin.bugs.rest.service.impl;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.repository.UserRepository;
import me.kapustin.bugs.rest.security.auth.Token;
import me.kapustin.bugs.rest.security.crypto.hash.Hasher;
import me.kapustin.bugs.rest.service.AuthService;
import me.kapustin.bugs.rest.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sun.security.krb5.Credentials;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Base64;
import java.util.Optional;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Service
public class AuthServiceImpl implements AuthService{

    @Autowired
    UserRepository userRepo;

    @Autowired
    TokenService tokenService;

    @Autowired
    Hasher hash;

    @SneakyThrows
    @Override
    public Token generateToken(String email, String password) {
        Optional<UserEntity> user = userRepo.findUserByEmail(email);
        UserEntity userEntity = user.orElseThrow(
                () -> new AuthenticationServiceException("Invalid credentials."));
        String sha512 = hash.hash(password, userEntity);
        if (sha512.equals(userEntity.getPasswordHash())) {
            Token token = Token.builder()
                    .email(email)
                    .password(password)
                    .build();
            token.setLoginTime(Instant.now());
            token.setHash(tokenService.tokenHash(token));
            return token;
        } else {
            throw new AuthenticationServiceException("Invalid credentials");
        }
    }

    @Override
    public Boolean authenticateWithToken(Token token) {
        Optional<UserEntity> user = userRepo.findUserByEmail(token.getEmail());
        UserEntity userEntity = user.orElseThrow(
                () -> new AuthenticationServiceException("Invalid credentials."));
        String sha512 = hash.hash(token.getPassword(), userEntity);
        if (sha512.equals(userEntity.getPasswordHash())) {
            return true;
        } else {
            return false;
        }
    }

}
