package me.kapustin.bugs.rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class CreateBugDTO {


    @NotNull
    @Getter @Setter private String name;

    @NotNull
    @Getter @Setter private String description;

    @NotNull
    @Getter @Setter private String priority;

    @NotNull
    @Getter @Setter private Integer projectId;

    @Getter @Setter private Integer assignFixerUserId;

    @NotNull
    @Getter @Setter private String foundIn;
}
