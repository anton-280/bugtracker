package me.kapustin.bugs.rest.entity;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Entity
@Table(name="priorities")
public class PriorityEntity extends AbstractEntity {

	public PriorityEntity() {
	}

	@Getter @Setter private String lang;

	@Getter @Setter private String name;
}