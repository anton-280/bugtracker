package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.controller.UserRestController;
import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.exception.*;
import me.kapustin.bugs.rest.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@RestController
@RequestMapping("/rest")
public class UserRestControllerImpl implements UserRestController {

    private static final Logger log = Logger.getLogger(UserRestControllerImpl.class);

    @Autowired
    UserService userService;

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public void createUser(@RequestBody UserDTO user, HttpServletRequest httpReq,
                           HttpServletResponse httpRes) {
        try {
            Integer newUserId = userService.registerUser(user).getId();
            httpRes.setStatus(HttpStatus.CREATED.value());
            httpRes.setHeader("Location", httpReq.getRequestURL().toString().replace("add",
                    newUserId.toString()));
        } catch (BeanValidationException bve) {
            httpRes.setStatus(HttpStatus.BAD_REQUEST.value());
            httpRes.setHeader("Warning", bve.getMessage());
        } catch (UserCreationException uce) {
            httpRes.setStatus(HttpStatus.CONFLICT.value());
            httpRes.setHeader("Warning", uce.getMessage());
        }
    }

    @Override
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody UserDTO getUserById(@PathVariable Integer id,
                        HttpServletResponse httpRes) {
        return userService.getById(id).map(UserEntity::toUserDTO)
                .orElseThrow(() -> {
                    httpRes.setStatus(HttpStatus.NOT_FOUND.value());
                    httpRes.setHeader("Warning", String.format("No user exist with id = %s.", id));
                    return new NoSuchUserException();
                });
    }

}