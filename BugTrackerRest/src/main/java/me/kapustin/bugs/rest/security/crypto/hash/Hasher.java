package me.kapustin.bugs.rest.security.crypto.hash;

import me.kapustin.bugs.rest.security.Saltable;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface Hasher {
    String hash(String pass, Saltable salt);
    String hash(String pass, String salt);
}
