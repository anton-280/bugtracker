package me.kapustin.bugs.rest.utils;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public final class HashCodeUtils {

    private HashCodeUtils(){
    }

    public static int generateHash(Object... objects) {
        int result = 1;
        int prime = 29;
        for (Object object : objects) {
            result = prime * result + object.hashCode();
        }
        return result;
    }
}