package me.kapustin.bugs.rest.service.impl;

import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.entity.BugEntity;
import me.kapustin.bugs.rest.entity.ProjectEntity;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.repository.*;
import me.kapustin.bugs.rest.service.BugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Optional;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class BugServiceImpl implements BugService {

    @Autowired
    UserRepository userRepo;

    @Autowired
    BugRepository bugRepo;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    VersionRepository versionRepository;

    @Autowired
    PriorityRepository priorityRepository;

    @Autowired
    StatusRepository statusRepository;


    SecurityContext securityContext = SecurityContextHolder.getContext();

    @Override
    public List<BugEntity> getOpenedBugs() {
        String email = securityContext.getAuthentication().getName();
        Optional<UserEntity> user = userRepo.findUserByEmail(email);
        return user.get().getOpened();
    }

    @Override
    public BugEntity createBug(CreateBugDTO bug) {
        String email = securityContext.getAuthentication().getName();
        Optional<UserEntity> user = userRepo.findUserByEmail(email);
        BugEntity bugEnt = new BugEntity();
        bugEnt.setName(bug.getName());
        bugEnt.setDescription(bug.getDescription());
        bugEnt.setAssignFixer(userRepo.findOne(bug.getAssignFixerUserId()));
        bugEnt.setDiscoverer(user.get());
        ProjectEntity project = projectRepository.findOne(bug.getProjectId());
        bugEnt.setFoundIn(versionRepository.findVersionByNameAndProject(bug.getFoundIn(),
                project));
        bugEnt.setPriority(priorityRepository.findPriorityByName(bug.getPriority()));
        bugEnt.setStatusBean(statusRepository.findStatusByName("New"));
        return bugRepo.save(bugEnt);
    }

    @Override
    public Optional<BugEntity> getBugById(int id) {
        return bugRepo.findOne(id);
    }
}
