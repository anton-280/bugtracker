package me.kapustin.bugs.rest.validate.impl;

import me.kapustin.bugs.rest.exception.BeanValidationException;
import me.kapustin.bugs.rest.validate.BeanValidator;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@Component
public class JavaxBeanValidator implements BeanValidator {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Override
    public void validate(Object object) {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);
        if (constraintViolations.size() != 0) {
            StringBuilder message = new StringBuilder();
            for (ConstraintViolation<Object> cv : constraintViolations) {
                message.append(String.format(
                        "Validation Error! property: [%s], value: [%s], message: [%s] \n",
                        cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
            }
            throw new BeanValidationException("Validation failed: "  + message.toString());
        }
    }

}
