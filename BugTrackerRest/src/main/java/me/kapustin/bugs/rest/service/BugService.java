package me.kapustin.bugs.rest.service;

import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.entity.BugEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public interface BugService {
    List<BugEntity> getOpenedBugs();

    BugEntity createBug(CreateBugDTO bug);

    Optional<BugEntity> getBugById(int id);
}
