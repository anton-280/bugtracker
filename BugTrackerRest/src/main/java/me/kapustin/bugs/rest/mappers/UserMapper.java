package me.kapustin.bugs.rest.mappers;

import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@Mapper
public interface UserMapper {
    BugMapper INSTANCE = Mappers.getMapper(BugMapper.class);

    UserDTO userEntityToUserDTO(UserEntity user);

    UserEntity userDTOToUserEntity(UserDTO user);
}
