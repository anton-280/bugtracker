package me.kapustin.bugs.rest.validate.impl;

import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.exception.BeanValidationException;
import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;
/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class JavaxBeanValidatorTest {

    UserDTO valid;

    UserDTO invalid;

    @Before
    public void init(){
        valid = new UserDTO();
        valid.setEmail("valid@email.com");
        valid.setName("Name");
        valid.setSurname("Surname");
        valid.setPassword("Asdewd43EDewd2342dwf4.2233");
        invalid = new UserDTO();
        invalid.setEmail("email@co,invalid");
        invalid.setName(null);
        invalid.setSurname(null);
        invalid.setPassword("qwe123");
    }

    @Test
    public void shouldNotThrownBeanValidationExceptionWhenValidateValidDTOOnMethodValidate() throws Exception {
        JavaxBeanValidator validator = new JavaxBeanValidator();
        validator.validate(valid);
    }

    @Test(expected = BeanValidationException.class)
    public void shouldThrownBeanValidationExceptionWhenValidateinValidDTOOnMethodValidate() throws Exception {
        JavaxBeanValidator validator = new JavaxBeanValidator();
        validator.validate(invalid);
    }

    @Test
    public void shouldThrownBeanValidationExceptionWithCorrectMessageWhenValidateValidDTOOnMethodValidate()
            throws Exception {
        JavaxBeanValidator validator = new JavaxBeanValidator();
        String message = "";
        try {
            validator.validate(invalid);
        } catch (BeanValidationException bve) {
            message = bve.getMessage();
        }
        assertThat(message).isNotNull().isNotEmpty();
    }

}