package me.kapustin.bugs.rest.service.impl;

import com.google.common.hash.Hashing;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.exception.DataCorruptedException;
import me.kapustin.bugs.rest.repository.UserRepository;
import me.kapustin.bugs.rest.security.Saltable;
import me.kapustin.bugs.rest.security.auth.Token;
import me.kapustin.bugs.rest.security.crypto.hash.Hasher;
import me.kapustin.bugs.rest.service.TokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationServiceException;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthServiceImplTest {

    @InjectMocks
    AuthServiceImpl authService;

    @Mock
    UserRepository userRepo;

    @Mock
    Hasher hasherMock;

    @Mock
    TokenService tokenServiceMock;

    UserEntity userEntity;


    @Before
    public void init() throws Exception {
        userEntity = new UserEntity();
        userEntity.setEmail("username@user.com");
        userEntity.setPasswordHash("4F8F5CB531E3D44A61CF417CD133792CCFA501FD8DA53EE368FED20E5FE0248C" +
                "3A0B64F98A6533CEE1DA614C3A8DDEC791FF05FEE6D971D57C1348320F4EB42D");
        userEntity.setSalt("ABBARARARARARAARSALT");
    }

    @Test
    public void shouldReturnValidTokenWhenAuthIsOkOnMethodGenerateToken() throws Exception {
        when(userRepo.findUserByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(hasherMock.hash(anyString(), any(Saltable.class))).thenReturn("4F8F5CB531E3D44A61CF417CD133792CCFA501FD8DA53EE368FED20E5FE0248C" +
                "3A0B64F98A6533CEE1DA614C3A8DDEC791FF05FEE6D971D57C1348320F4EB42D");
        when(tokenServiceMock.tokenHash(any(Token.class))).thenReturn("6A0B64F98A6533CEE1DA614C3A8DDEC7");
        Token token = authService.generateToken("username@user.com", "pa55Word");
        assertThat(token).isNotNull();
        assertThat(token.getEmail()).isNotNull().isNotEmpty().isEqualTo("username@user.com");
        assertThat(token.getPassword()).isNotNull().isNotEmpty().isEqualTo("pa55Word");
        assertThat(token.getLoginTime()).isNotNull();
        assertThat(token.getHash()).isNotNull().isNotEmpty();
        verify(userRepo).findUserByEmail(anyString());
        verify(hasherMock).hash(anyString(), any(Saltable.class));
        verify(tokenServiceMock).tokenHash(any(Token.class));
    }



    @Test(expected = AuthenticationServiceException.class)
    public void shouldThrowAuthenticationExceptionWhenPassInvalidOnMethodGenerateToken() throws Exception {
        when(userRepo.findUserByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(hasherMock.hash(anyString(), any(Saltable.class))).thenReturn("Any Other Hash...");
        Token token = authService.generateToken("username@user.com", "InvalidPa55Word");
        verify(userRepo).findUserByEmail(anyString());
        verify(hasherMock).hash(anyString(), any(Saltable.class));
    }

    @Test(expected = AuthenticationServiceException.class)
    public void shouldThrowAuthenticationExceptionWhenNoUserWithSameEmailOnMethodGenerateToken() throws Exception {
        when(userRepo.findUserByEmail(anyString())).thenReturn(Optional.empty());
        Token token = authService.generateToken("invalidUsername@user.com", "InvalidPa55Word");
        verify(userRepo).findUserByEmail(anyString());
    }

    @Test
    public void shouldReturnTrueWhenAuthWithValidCredentialsOnMethodAuthenticateWithToken() throws Exception {
        Instant now = Instant.now();
        Token testToken = Token.builder()
                .email("testvalid@email.com")
                .password("testesdfsdfet")
                .build();
        testToken.setLoginTime(now);
        when(userRepo.findUserByEmail(testToken.getEmail())).thenReturn(Optional.of(userEntity));
        when(hasherMock.hash(testToken.getPassword(), userEntity)).thenReturn(userEntity.getPasswordHash());
        Boolean isAuth = authService.authenticateWithToken(testToken);
        assertThat(isAuth).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenAuthWithInvalidCredentialsOnMethodAuthenticateWithToken() throws Exception {
        Token testToken = Token.builder()
                .email("testvalid@email.com")
                .password("testesdfsdfet")
                .build();
        testToken.setLoginTime(Instant.now());
        testToken.setHash(new TokenServiceImpl().tokenHash(testToken));
        testToken.setLoginTime(Instant.now());
        when(userRepo.findUserByEmail(testToken.getEmail())).thenReturn(Optional.of(userEntity));
        when(hasherMock.hash(testToken.getPassword(), userEntity)).thenReturn("OTHER ANY HASH");
        Boolean isAuth = authService.authenticateWithToken(testToken);
        assertThat(isAuth).isFalse();
    }
}
