package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.security.auth.Token;
import me.kapustin.bugs.rest.service.AuthService;
import me.kapustin.bugs.rest.service.TokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationServiceException;

import java.time.Instant;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthRestControllerImplTest {

    @InjectMocks
    AuthRestControllerImpl authRestController;

    @Mock
    AuthService authServiceMock;

    @Mock
    TokenService tokenServiceMock;

    MockHttpServletResponse res = new MockHttpServletResponse();

    @Test
    public void shouldReturn200HTTPStatusAndGeneratedValidTokenInHeaderWhenAuthIsOkOnMethodAuthenticate()
            throws Exception {
        res.setStatus(0);
        String email = "user@email.com";
        String password = "password";
        Token token = Token.builder().email(email).password(password)
                .hash("TESTHASH").build();
        token.setLoginTime(Instant.now());
        when(authServiceMock.generateToken(email, password)).thenReturn(token);
        when(tokenServiceMock.encryptToken(token)).thenReturn("sad3dfdnfsudf9w84n3bge0rfibwerghergewrguiergudfsgdufgihwu4phgiuweghreughwuergdflsblgjlsb4b93ho3qb;eqgb;===");
        String encodeToken = authRestController.authenticate(email, password, res);
        assertThat(res.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(encodeToken).isNotNull().isNotEmpty();
        verify(authServiceMock, times(1)).generateToken(email, password);
        verify(tokenServiceMock, times(1)).encryptToken(token);
    }

    @Test
    public void shouldReturn401HTTPStatusAndWarningInHeaderWhenAuthFailOnMethodAuthenticate() throws Exception {
        String email = "user@email.com";
        String password = "password";
        when(authServiceMock.generateToken(email, password)).thenThrow(new AuthenticationServiceException("Invalid credentials"));
        String encodeToken = authRestController.authenticate(email, password, res);
        assertThat(res.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(encodeToken).isEmpty();
        assertThat(res.containsHeader("Warning")).isTrue();
        verify(authServiceMock, times(1)).generateToken(email, password);
    }
}
