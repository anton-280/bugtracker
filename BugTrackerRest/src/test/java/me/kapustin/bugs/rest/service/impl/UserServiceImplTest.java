package me.kapustin.bugs.rest.service.impl;

import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.exception.BeanValidationException;
import me.kapustin.bugs.rest.exception.UserCreationException;
import me.kapustin.bugs.rest.repository.UserRepository;
import me.kapustin.bugs.rest.security.Saltable;
import me.kapustin.bugs.rest.security.crypto.hash.Hasher;
import me.kapustin.bugs.rest.validate.BeanValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepositoryMock;

    @Mock
    BeanValidator beanValidatorMock;

    @Mock
    Hasher hasherMock;

    UserDTO user;

    @Before
    public void init() throws Exception {
        user = new UserDTO();
        user.setEmail("test@test.com");
        user.setName("John");
        user.setSurname("Mocker");
        user.setPassword("paasswwword");
    }

    @Test(expected = UserCreationException.class)
    public void shouldThrowUCEWhenUserWithSameEmailExistsOnMethodRegisterUser() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1);
        userEntity.setEmail("test@test.com");
        when(userRepositoryMock.save(any(UserEntity.class))).thenReturn(userEntity);
        when(userRepositoryMock.findUserByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(hasherMock.hash(anyString(), any(Saltable.class)))
                .thenReturn("4F8F5CB531E3D44A61CF417CD133792CCFA501FD8DA53EE368FED20E5FE0248C" +
                        "3A0B64F98A6533CEE1DA614C3A8DDEC791FF05FEE6D971D57C1348320F4EB42D");
        userService.registerUser(user);
        verify(userRepositoryMock).save(any(UserEntity.class));
        verify(userRepositoryMock).findUserByEmail(anyString());
        verify(hasherMock).hash(anyString(), any(Saltable.class));
    }

    @Test(expected = BeanValidationException.class)
    public void shouldFailWhenValidatorThrowExceptionOnMethodRegisterUser() throws Exception {
        doThrow(new BeanValidationException("")).when(beanValidatorMock).validate(any(UserDTO.class));
        userService.registerUser(new UserDTO());
        verify(beanValidatorMock).validate(any(UserDTO.class));
    }

    @Test
    public void shouldCreateUserWhenUserWithSameEmailNotExistsOnMethodRegisterUser() throws Exception {
        when(userRepositoryMock.save(any(UserEntity.class)))
                .thenAnswer((Answer<UserEntity>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (UserEntity) args[0];
                });
        when(userRepositoryMock.findUserByEmail(anyString())).thenReturn(Optional.empty());
        when(hasherMock.hash(anyString(), any(Saltable.class)))
                .thenReturn("4F8F5CB531E3D44A61CF417CD133792CCFA501FD8DA53EE368FED20E5FE0248C" +
                        "3A0B64F98A6533CEE1DA614C3A8DDEC791FF05FEE6D971D57C1348320F4EB42D");
        stub(hasherMock.hash("aaaa", "aaa")).toReturn("dffsf");
        UserEntity saved = userService.registerUser(user);
        assertThat(saved.getEmail()).isEqualTo(user.getEmail());
        assertThat(saved.getName()).isEqualTo(user.getName());
        assertThat(saved.getSurname()).isEqualTo(user.getSurname());
        assertThat(saved.getPasswordHash()).isNotNull().isNotEmpty();
        assertThat(saved.getPasswordHash().length()).isEqualTo(128);
        assertThat(saved.getSalt()).isNotEmpty().isNotNull();
        assertThat(saved.isActive()).isFalse();
        boolean isNotOnlyNull = false;
        for (byte b : saved.getSalt().getBytes()) {
            if (b != 0) {
                isNotOnlyNull = true;
            }
        }
        assertTrue(isNotOnlyNull);
        verify(userRepositoryMock).save(any(UserEntity.class));
        verify(userRepositoryMock).findUserByEmail(anyString());
        verify(hasherMock).hash(anyString(), any(Saltable.class));
    }

}