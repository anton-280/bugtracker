package me.kapustin.bugs.rest.security.auth;

import me.kapustin.bugs.rest.exception.DataCorruptedException;
import me.kapustin.bugs.rest.service.AuthService;
import me.kapustin.bugs.rest.service.TokenService;
import me.kapustin.bugs.rest.service.impl.TokenServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import javax.servlet.FilterChain;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class TokenAuthenticationFilterTest {

    @InjectMocks
    TokenAuthenticationFilter authenticationFilter;

    @Mock
    private TokenService tokenServiceMock;

    @Mock
    private AuthService authServiceMock;

    @Mock
    private FilterChain chainMock;

    @Mock
    private SecurityContext securityContext;

    MockHttpServletRequest mockReq = new MockHttpServletRequest();

    MockHttpServletResponse mockRes = new MockHttpServletResponse();

    Token testToken;

    String encodedToken;

    @Before
    public void setUp() throws Exception {
        testToken = Token.builder()
                .email("testvalid@email.com")
                .password("testesdfsdfet")
                .build();
        testToken.setLoginTime(Instant.now());
        testToken.setHash(new TokenServiceImpl().tokenHash(testToken));
        encodedToken = new TokenServiceImpl().encryptToken(testToken);
    }

    @Test
    public void shouldPassAuthWhenCredentialsAreValidOnMethodDoFilter() throws Exception {
        mockReq.addHeader("X-Auth-Token", encodedToken);
        when(securityContext.getAuthentication()).thenReturn(null);
        when(tokenServiceMock.decryptToken(encodedToken)).thenReturn(testToken);
        when(authServiceMock.authenticateWithToken(testToken)).thenReturn(true);
        doNothing().when(securityContext).setAuthentication(any(Authentication.class));
        doNothing().when(chainMock).doFilter(mockReq, mockRes);
        authenticationFilter.doFilter(mockReq, mockRes, chainMock);
        verify(securityContext, times(1)).getAuthentication();
        verify(tokenServiceMock, times(1)).decryptToken(encodedToken);
        verify(authServiceMock, times(1)).authenticateWithToken(testToken);
        verify(securityContext, times(1)).setAuthentication(any(Authentication.class));
        verify(chainMock, times(1)).doFilter(mockReq, mockRes);
    }

    @Test
    public void shouldReturn401StatusCodeAuthWhenCredentialsAreInvalidWhenOnMethodDoFilter() throws Exception {
        mockReq.addHeader("X-Auth-Token", encodedToken);
        when(securityContext.getAuthentication()).thenReturn(null);
        when(tokenServiceMock.decryptToken(encodedToken)).thenReturn(testToken);
        when(authServiceMock.authenticateWithToken(testToken)).thenReturn(false);
        doNothing().when(securityContext).setAuthentication(any(Authentication.class));
        doNothing().when(chainMock).doFilter(mockReq, mockRes);
        authenticationFilter.doFilter(mockReq, mockRes, chainMock);
        verify(securityContext, times(1)).getAuthentication();
        verify(tokenServiceMock, times(1)).decryptToken(encodedToken);
        verify(authServiceMock, times(1)).authenticateWithToken(testToken);
        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verify(chainMock, times(1)).doFilter(mockReq, mockRes);
        assertThat(mockRes.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(mockRes.getContentAsString()).isNotNull().isNotEmpty()
                .isEqualTo("Authentication failed, invalid credentials.");
    }

    @Test
    public void shouldReturn400HttpStatusAuthWhenTokenDataCorruptedWhenOnMethodDoFilter() throws Exception {
        mockReq.addHeader("X-Auth-Token", "Invalid hash...");
        when(securityContext.getAuthentication()).thenReturn(null);
        when(tokenServiceMock.decryptToken("Invalid hash...")).thenThrow(new DataCorruptedException());
        when(authServiceMock.authenticateWithToken(testToken)).thenReturn(true);
        doNothing().when(securityContext).setAuthentication(any(Authentication.class));
        doNothing().when(chainMock).doFilter(mockReq, mockRes);
        authenticationFilter.doFilter(mockReq, mockRes, chainMock);
        verify(securityContext, times(1)).getAuthentication();
        verify(tokenServiceMock, times(1)).decryptToken("Invalid hash...");
        verify(authServiceMock, times(0)).authenticateWithToken(testToken);
        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verify(chainMock, times(1)).doFilter(mockReq, mockRes);
        assertThat(mockRes.getStatus()).isEqualTo(400);
        assertThat(mockRes.getContentAsString()).isNotNull()
                .isNotEmpty();
    }

    @Test
    public void shouldInvokeChainDoFilterWhenUserAlreadyAuthenticatedOnMethodDoFilter() throws Exception {
        mockReq.addHeader("X-Auth-Token", "Invalid hash...");
        when(securityContext.getAuthentication()).thenReturn(anyObject());
        when(tokenServiceMock.decryptToken("Invalid hash...")).thenThrow(new DataCorruptedException());
        when(authServiceMock.authenticateWithToken(testToken)).thenReturn(true);
        doNothing().when(securityContext).setAuthentication(any(Authentication.class));
        doNothing().when(chainMock).doFilter(mockReq, mockRes);
        authenticationFilter.doFilter(mockReq, mockRes, chainMock);
        verify(securityContext, times(1)).getAuthentication();
        verify(tokenServiceMock, times(1)).decryptToken("Invalid hash...");
        verify(authServiceMock, times(0)).authenticateWithToken(testToken);
        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verify(chainMock, times(1)).doFilter(mockReq, mockRes);
        assertThat(mockRes.getStatus()).isEqualTo(400);
        assertThat(mockRes.getContentAsString()).isNotNull()
                .isNotEmpty();
    }

    @Test
    public void shouldReturn401HttpStatusWhenNoTokenInHeaderOnMethodDoFilter() throws Exception {
        when(securityContext.getAuthentication()).thenReturn(null);
        when(tokenServiceMock.decryptToken(null)).thenThrow(new IllegalArgumentException());
        when(authServiceMock.authenticateWithToken(testToken)).thenReturn(true);
        doNothing().when(securityContext).setAuthentication(any(Authentication.class));
        doNothing().when(chainMock).doFilter(mockReq, mockRes);
        authenticationFilter.doFilter(mockReq, mockRes, chainMock);
        verify(securityContext, times(1)).getAuthentication();
        verify(tokenServiceMock, times(1)).decryptToken(null);
        verify(authServiceMock, times(0)).authenticateWithToken(testToken);
        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verify(chainMock, times(1)).doFilter(mockReq, mockRes);
        assertThat(mockRes.getStatus()).isEqualTo(401);
        assertThat(mockRes.getContentAsString()).isNotNull()
                .isNotEmpty();
    }
}