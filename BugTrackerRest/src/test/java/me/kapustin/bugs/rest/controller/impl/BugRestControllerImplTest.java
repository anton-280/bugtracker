package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.*;
import me.kapustin.bugs.rest.exception.BeanValidationException;
import me.kapustin.bugs.rest.service.BugService;
import me.kapustin.bugs.rest.validate.BeanValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class BugRestControllerImplTest {

    @InjectMocks
    private BugRestControllerImpl underTest;

    @Mock
    BugService  bugService;

    @Mock
    BeanValidator beanValidatorMock;

    MockHttpServletRequest mockReq = new MockHttpServletRequest();

    MockHttpServletResponse mockRes = new MockHttpServletResponse();

    ArrayList<BugEntity> testBugs = new ArrayList<>();

    CreateBugDTO bugDTO;

    @Before
    public void setUp() throws Exception {
        mockRes.setStatus(0);
        BugEntity bug1 = new BugEntity();
        BugEntity bug2 = new BugEntity();
        BugEntity bug3 = new BugEntity();
        UserEntity currentUser = new UserEntity(){{setName("Tester Current");}};
        bug1.setDiscoverer(currentUser);
        bug2.setDiscoverer(currentUser);
        bug3.setDiscoverer(currentUser);
        bug1.setName("Bug 1");
        bug2.setName("Bug 2");
        bug3.setName("Bug 3");
        bug1.setId(1);
        bug2.setId(2);
        bug3.setId(3);
        bug1.setDescription("Desc 1");
        bug2.setDescription("Desc 2");
        bug3.setDescription("Desc 3");
        bug1.setPriority(new PriorityEntity(){{setName("Flaming");}});
        bug2.setPriority(new PriorityEntity(){{setName("Flaming");}});
        bug3.setPriority(new PriorityEntity(){{setName("Flaming");}});
        bug1.setStatusBean(new StatusEntity(){{setName("Fixed");}});
        bug2.setStatusBean(new StatusEntity(){{setName("New");}});
        bug3.setStatusBean(new StatusEntity(){{setName("Closed");}});
        bug1.setFoundIn(new VersionEntity(){{setName("0.1.5");}});
        bug2.setFoundIn(new VersionEntity(){{setName("0.1.5");}});
        bug3.setFoundIn(new VersionEntity(){{setName("0.1.5");}});
        bug1.setFixed(new VersionEntity(){{setName("1.2.9");}});
        bug1.setProject(new ProjectEntity(){{setName("test PRoJeeeeect");}});
        testBugs.add(bug1);
        testBugs.add(bug2);
        testBugs.add(bug3);
        bugDTO = new CreateBugDTO();
        bugDTO.setName("NEW VERY IMPORTANT BUG");
        bugDTO.setProjectId(1);
        bugDTO.setFoundIn("0.0.1");
        bugDTO.setAssignFixerUserId(2);
        bugDTO.setDescription("FIX IT AAAAA");
        bugDTO.setPriority("Blocker");
    }

    @Test
    public void shouldReturn200HttpStatusWhenOpenedBugListSuccessfullyOnMethodGetOpenedBugs() throws Exception {
        when(bugService.getOpenedBugs()).thenReturn(testBugs);
        List<BugEntity> bugEntitiesOpenedByCurrentUser = underTest.getUsersBugs();
        assertThat(bugEntitiesOpenedByCurrentUser, hasSize(3));
        verify(bugService).getOpenedBugs();
    }

    @Test
    public void shouldReturn200HttpStatusAndEmptyListWhenNoOpenedBugOnMethodGetOpenedBugs() throws Exception {
        when(bugService.getOpenedBugs()).thenReturn(null);
        List<BugEntity> bugEntitiesOpenedByCurrentUser = underTest.getUsersBugs();
        assertNotNull(bugEntitiesOpenedByCurrentUser);
        assertThat(bugEntitiesOpenedByCurrentUser, hasSize(0));
        verify(bugService).getOpenedBugs();
    }

    @Test
    public void shouldReturn201HttpStatusAndContainLocationHeaderWhenBugSuccessfullyCreatedOnMethodOpenBug()
            throws Exception {
        mockReq.setRequestURI("https://localhost:8800/rest/bug/open");
        when(bugService.createBug(bugDTO)).thenReturn(new BugEntity(){{setId(11);}});
        underTest.openBug(bugDTO, mockReq, mockRes);
        assertThat(mockRes.containsHeader("Location"),is(true));
        assertThat(mockRes.getStatus(), is(201));
        assertThat(mockRes.getHeader("Location"), is("https://localhost:8800/rest/bug/11"));
        verify(bugService).createBug(bugDTO);
    }

    @Test
    public void shouldReturn400HttpStatusAndContainWarningHeaderWhenValidationFailOnMethodOpenBug()
            throws Exception {
        bugDTO.setName(null);
        when(bugService.createBug(bugDTO)).thenReturn(new BugEntity(){{setId(11);}});
        doThrow(new BeanValidationException("")).when(beanValidatorMock).validate(any(UserDTO.class));
        underTest.openBug(bugDTO, mockReq, mockRes);
        assertThat(mockRes.containsHeader("Warning"),is(true));
        assertThat(mockRes.getStatus(), is(400));
        verify(bugService, times(0)).createBug(bugDTO);
    }

    @Test
    public void shouldReturn200HttpStatusWhenBugExistsOnMethodGetBug() throws Exception {
        when(bugService.getBugById(11)).thenReturn(Optional.of(new BugEntity(){{setId(11);}}));
        BugEntity bugEntity = underTest.getBug(11, mockRes);
        assertThat(mockRes.getStatus(), is(200));
        verify(bugService).getBugById(11);
    }
}
