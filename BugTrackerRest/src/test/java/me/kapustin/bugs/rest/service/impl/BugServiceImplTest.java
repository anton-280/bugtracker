package me.kapustin.bugs.rest.service.impl;

import me.kapustin.bugs.rest.dto.CreateBugDTO;
import me.kapustin.bugs.rest.entity.*;
import me.kapustin.bugs.rest.exception.NoSuchBugException;
import me.kapustin.bugs.rest.repository.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;


/**
 * @author Kapustin Anton anton@kapustin.me
 */
@RunWith(MockitoJUnitRunner.class)
public class  BugServiceImplTest {


    @InjectMocks
    BugServiceImpl bugService;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private SecurityContext securityContextMock;

    @Mock
    private BugRepository bugRepositoryMock;

    @Mock
    private Authentication authMock;

    @Mock
    private ProjectRepository projectRepositoryMock;

    @Mock
    private VersionRepository versionRepositoryMock;

    @Mock
    private PriorityRepository priorityRepositoryMock;

    @Mock
    private StatusRepository statusRepositoryMock;


    UserEntity userEntity = new UserEntity();

    @Before
    public void init() {
        userEntity.setId(1);
        userEntity.setEmail("test@test.com");
    }

    @Test
    public void shouldReturnAllOpenedBugsForUserOnMethodGetOpenedBugs() throws Exception {
        ArrayList<BugEntity> testBugs = new ArrayList<>();
        BugEntity bug1 = new BugEntity();
        bug1.setName("Bug 1");
        BugEntity bug2 = new BugEntity();
        bug2.setName("Bug 2");
        BugEntity bug3 = new BugEntity();
        bug3.setName("Bug 3");
        userEntity.setOpened(testBugs);
        testBugs.add(bug1);
        testBugs.add(bug2);
        testBugs.add(bug3);
        when(securityContextMock.getAuthentication()).thenReturn(authMock);
        when(authMock.getName()).thenReturn(userEntity.getEmail());
        when(userRepositoryMock.findUserByEmail(userEntity.getEmail())).thenReturn(Optional.of(userEntity));
        List<BugEntity> bugs = bugService.getOpenedBugs();
        assertThat(bugs, containsInAnyOrder(bug1, bug2, bug3));
        assertThat(bugs, hasSize(3));
        verify(securityContextMock, times(1)).getAuthentication();
        verify(authMock, times(1)).getName();
        verify(userRepositoryMock, times(1)).findUserByEmail(userEntity.getEmail());
    }


    @Test
    public void shouldReturnBugIdWhenBugSuccessfullyCreatedOnMethodCreateBug() throws Exception {
        CreateBugDTO bug = new CreateBugDTO();
        bug.setName("New very important bug");
        bug.setDescription("Description very important bug...");
        bug.setPriority("Flaming");
        bug.setAssignFixerUserId(userEntity.getId());
        bug.setFoundIn("V0.0.1DEV");
        bug.setProjectId(1);
        BugEntity bugEntity = new BugEntity();
        when(securityContextMock.getAuthentication()).thenReturn(authMock);
        when(authMock.getName()).thenReturn(userEntity.getEmail());
        when(userRepositoryMock.findUserByEmail(userEntity.getEmail())).thenReturn(Optional.of(userEntity));
        when(userRepositoryMock.findOne(bug.getAssignFixerUserId())).thenReturn(userEntity);
        ProjectEntity projectEntity = new ProjectEntity() {{
            setId(bug.getProjectId());
        }};
        when(projectRepositoryMock.findOne(bug.getProjectId())).thenReturn(projectEntity);
        when(versionRepositoryMock.findVersionByNameAndProject(bug.getFoundIn(), projectEntity))
                .thenReturn(new VersionEntity(){{setName(bug.getFoundIn());}});
        when(priorityRepositoryMock.findPriorityByName(bug.getPriority())).thenReturn(new PriorityEntity(){{
            setName(bug.getPriority());
        }});
        when(statusRepositoryMock.findStatusByName("New")).thenReturn(new StatusEntity(){{setName("New");}});
        when(bugRepositoryMock.save(any(BugEntity.class))).thenAnswer((Answer<BugEntity>) invocation -> {
            Object[] args = invocation.getArguments();
            return (BugEntity) args[0];
        });
        BugEntity createdBug = bugService.createBug(bug);
        assertThat(createdBug.getId(), is(bugEntity.getId()));
        assertThat(createdBug.getName(), is(bug.getName()));
        assertThat(createdBug.getDescription(), is(bug.getDescription()));
        assertThat(createdBug.getAssignFixer(), is(userEntity));
        assertThat(createdBug.getDiscoverer(), is(userEntity));
        assertThat(createdBug.getFoundIn().getName(), is(bug.getFoundIn()));
        assertThat(createdBug.getPriority().getName(), is(bug.getPriority()));
        assertThat(createdBug.getStatusBean().getName(), is("New"));
        verify(securityContextMock, times(1)).getAuthentication();
        verify(authMock, times(1)).getName();
        verify(userRepositoryMock, times(1)).findUserByEmail(userEntity.getEmail());
        verify(userRepositoryMock, times(1)).findOne(bug.getAssignFixerUserId());
        verify(bugRepositoryMock, times(1)).save(any(BugEntity.class));
        verify(projectRepositoryMock, times(1)).findOne(bug.getProjectId());
        verify(versionRepositoryMock, times(1)).findVersionByNameAndProject(bug.getFoundIn(), projectEntity);
        verify(priorityRepositoryMock, times(1)).findPriorityByName(bug.getPriority());
        verify(statusRepositoryMock, times(1)).findStatusByName("New");
    }

    @Test
    public void returnBugById() throws Exception {
        when(bugRepositoryMock.findOne(anyInt())).thenReturn(Optional.of(new BugEntity(){{setId(1);}}));
        Optional<BugEntity> bugEnt = bugService.getBugById(1);
        verify(bugRepositoryMock, times(1)).findOne(anyInt());
    }
}
