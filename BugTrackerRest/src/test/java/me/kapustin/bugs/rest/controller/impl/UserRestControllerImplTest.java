package me.kapustin.bugs.rest.controller.impl;

import me.kapustin.bugs.rest.dto.UserDTO;
import me.kapustin.bugs.rest.entity.UserEntity;
import me.kapustin.bugs.rest.exception.*;
import me.kapustin.bugs.rest.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;


/**
 * @author Anton Kapustin antonkapystin@hotmail.com
 */
@RunWith(MockitoJUnitRunner.class)
public class UserRestControllerImplTest {

    @InjectMocks
    UserRestControllerImpl userRestController ;

    @Mock
    UserService mockUserService;

    @Test
    public void shouldReturn201StatusAndLocationHeaderWhenCreateValidUserOnMethodCreateUser() {
        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        mockReq.setRequestURI("https://localhost:8800/user/add");
        UserDTO user = new UserDTO();
        user.setEmail("test@test.com");
        user.setName("John");
        user.setSurname("Mocker");
        user.setPassword("paasswwword");
        when(mockUserService.registerUser(any(UserDTO.class))).thenReturn(new UserEntity() {{
            setId(1);
        }});
        userRestController.createUser(user, mockReq, mockRes);
        assertThat(mockRes.containsHeader("Location")).isTrue();
        assertEquals(mockRes.getStatus(), HttpStatus.CREATED.value());
        verify(mockUserService).registerUser(any(UserDTO.class));
    }

    @Test
    public void shouldReturn400StatusAndWarningHeaderWhenValidationFailOnMethodCreateUser() {
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        when(mockUserService.registerUser(any(UserDTO.class))).thenThrow(new BeanValidationException(""));
        userRestController.createUser(new UserDTO(), new MockHttpServletRequest(), mockRes);
        assertThat(mockRes.containsHeader("Warning")).isTrue();
        assertEquals(mockRes.getStatus(), HttpStatus.BAD_REQUEST.value());
        verify(mockUserService).registerUser(any(UserDTO.class));
    }

    @Test
    public void shouldReturn409StatusAndWarningHeaderWhenDuplicateUserOnMethodCreateUser() {
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        when(mockUserService.registerUser(any(UserDTO.class))).thenThrow(new UserCreationException(""));
        userRestController.createUser(new UserDTO(), new MockHttpServletRequest(), mockRes);
        assertThat(mockRes.containsHeader("Warning")).isTrue();
        assertEquals(mockRes.getStatus(), HttpStatus.CONFLICT.value());
        verify(mockUserService).registerUser(any(UserDTO.class));
    }

    @Test
    public void shouldReturn200StatusAndValidUserDTOWhenIdIsValidAndUserExistsOnMethodGetUserById() throws Exception {  UserRestControllerImpl userRestController = new UserRestControllerImpl();
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        UserEntity userEntity = new UserEntity();
        userEntity.setActive(false);
        userEntity.setSurname("test");
        userEntity.setName("test");
        userEntity.setEmail("test");
        userEntity.setId(1);
        when(mockUserService.getById(anyInt())).thenReturn(Optional.of(userEntity));
        userRestController.userService = mockUserService; //FIXME: Strange bug
        UserDTO userById = userRestController.getUserById(1, mockRes);
        assertThat(mockRes.getStatus()).isEqualTo(200);
        assertThat(userById.getId()).isEqualTo(1);
        verify(mockUserService).getById(anyInt());
    }


    @Test(expected = NoSuchUserException.class)
    public void shouldThrowNoSuchUserExceptionWhenUserNotExistsOnMethodGetUserById() throws Exception {
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        when(mockUserService.getById(anyInt())).thenReturn(Optional.empty());
        userRestController.getUserById(1, mockRes);
        verify(mockUserService).getById(anyInt());
    }

    @Test
    public void shouldReturn404StatusAndWarningHeaderWhenUserNotExistsOnMethodGetUserById() throws Exception {
        MockHttpServletResponse mockRes = new MockHttpServletResponse();
        when(mockUserService.getById(anyInt())).thenReturn(Optional.empty());
        boolean thrown = false;
        try {
            userRestController.getUserById(1, mockRes);
        } catch (NoSuchUserException nsue) {
            thrown = true;
        }
        assertTrue(thrown);
        assertThat(mockRes.getStatus()).isEqualTo(404);
        assertThat(mockRes.containsHeader("Warning")).isTrue();
        verify(mockUserService).getById(anyInt());
    }
}