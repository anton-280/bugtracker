package me.kapustin.bugs.rest.service.impl;

import com.google.common.hash.Hashing;
import me.kapustin.bugs.rest.exception.DataCorruptedException;
import me.kapustin.bugs.rest.security.auth.Token;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Kapustin Anton anton@kapustin.me
 */
public class TokenServiceImplTest {
    
    TokenServiceImpl tokenService = new TokenServiceImpl();

    Token testToken;

    @Before
    public void setUp() throws Exception {
        Instant now = Instant.now();
        testToken = Token.builder()
                .email("testvalid@email.com")
                .password("testesdfsdfet")
                .hash(Hashing.goodFastHash(32).hashString("testesdfsdfet" + "testvalid@email.com" + now.toString(),
                        Charset.forName("UTF8")).toString())
                .build();
        testToken.setLoginTime(now);
    }

    @Test
    public void shouldDontChangeTokenDataWhenEncryptAndDecryptTokenOnMethodsEncryptAndDecrypt() throws Exception {
        String cryptToken = tokenService.encryptToken(testToken);
        assertThat(cryptToken)
                .isNotEmpty()
                .isNotNull()
                .isNotEqualTo(testToken.toString());
        Token token = tokenService.decryptToken(cryptToken);
        assertThat(token).isEqualTo(testToken);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenNullPassedOnMethodDecrypt() throws Exception {
        tokenService.decryptToken(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenEmptyStringPassedOnMethodDecrypt() throws Exception {
        tokenService.decryptToken("");
    }

    @Test(expected = DataCorruptedException.class)
    public void shouldThrowDataCorruptedExceptionWhenHashNotEqReceiveHashOnMethodDecrypt() throws Exception {
        Token token = Token.builder()
                .hash("INVALID HASH...")
                .password("pass")
                .email("email")
                .build();
        token.setLoginTime(Instant.now());
        tokenService.decryptToken(tokenService.encryptToken(token));
    }

    @Test
    public void shouldReturnTokenValidHashOnMethodTokenHash() throws Exception {
        String tokenHash = tokenService.tokenHash(testToken);
        assertThat(tokenHash).isNotNull().isNotEmpty();
    }

    @Test
    public void shouldReturnTrueWhenValidateValidTokenOnMethodisHashValid() throws Exception {
        Boolean bool = tokenService.isHashValid(testToken);
        assertThat(bool).isTrue();
    }
    @Test
    public void shouldReturnFalseWhenCheckInvalidTokenOnMethodIsHashValid() throws Exception {
        Token invalidTestToken = Token.builder()
                .email("valid@email.co")
                .hash("INVALID HASh...")
                .password("pa55Word")
                .build();
        invalidTestToken.setLoginTime(Instant.now());
        Boolean bool = tokenService.isHashValid(invalidTestToken);
        assertThat(bool).isFalse();
    }
}
